mod error;
mod lexer;
mod parser;
mod interpreter;
mod src2src;

/// helper function for tests
fn run(input: &str) -> Result<(), error::ErrorType> {
    let lexer = lexer::Lexer::new(input);
    let mut parser = parser::Parser::new(lexer)?;
    let tree = parser.parse()?;
    let mut stb = interpreter::SemanticAnalyzer::new();
    stb.visit(&tree)?;
    println!("Symbol Table is {:?}", stb.scopes);
    let mut interpreter = interpreter::Interpreter::new();
    interpreter.interpret(&tree);
    println!("Global Memory is {:?}", interpreter.global_memory);
    Ok(())
}

/// Main
fn main() {
    match run(include_str!("../program.pas")) {
        Err(e) => println!("Error Occured: {:?}", e),
        _ => {}
    }
}
