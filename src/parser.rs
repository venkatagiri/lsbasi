use error::ErrorType;
use lexer::{
    TokenType,
    Token,
    Lexer,
};
use lexer::TokenType::*;

/// Nodes in AST
#[derive(Clone, Debug)]
pub enum Node {
    Program(Token, Box<Node>),
    ProcedureDecl(Token, Box<Node>, Box<Vec<Node>>),
    Block(Box<Vec<Node>>, Box<Node>),
    Param(Box<Node>, Box<Node>),
    VarDecl(Box<Node>, Box<Node>),
    TypeSpec(Token),
    BinaryOp(Box<Node>, Token, Box<Node>),
    UnaryOp(Token, Box<Node>),
    NumOp(Token),
    Compound(Box<Vec<Node>>),
    Assign(Box<Node>, Token, Box<Node>),
    Var(Token),
    NoOp(),
}

impl Node {
    pub fn unwrap(&self) -> Token {
        match self {
            &Node::TypeSpec(ref node) | &Node::Var(ref node) => {
                node.clone()
            },
            _ => unreachable!(),
        }
    }
}

/// Parser
#[derive(Clone, Debug)]
pub struct Parser {
    lexer: Lexer,
    current_token: Token,
}

impl Parser {
    pub fn new(mut lexer: Lexer) -> Result<Parser, ErrorType> {
        let ct = lexer.get_next_token()?;
        Ok(Parser {
            lexer: lexer,
            current_token: ct,
        })
    }

    /// Compare the current token type with the passed token
    /// type and if they match then "eat" the current token
    /// and assign the next token to the self.current_token,
    /// otherwise raise an exception.
    fn eat(&mut self, t: TokenType) -> Result<(), ErrorType> {
        if self.current_token.token_type != t {
            Err(ErrorType::InvalidToken(t.clone(), self.current_token.clone()))
        } else {
            self.current_token = self.lexer.get_next_token()?;
            Ok(())
        }
    }

    /// factor : PLUS factor | MINUS factor | INTEGER_CONST | REAL_CONST | LPAREN expr RPAREN | variable
    fn factor(&mut self) -> Result<Node, ErrorType> {
        match self.current_token.token_type {
            PLUS => {
                let op = self.current_token.clone();
                self.eat(PLUS)?;
                Ok(Node::UnaryOp(op, Box::new(self.factor()?)))
            },
            MINUS => {
                let op = self.current_token.clone();
                self.eat(MINUS)?;
                Ok(Node::UnaryOp(op, Box::new(self.factor()?)))
            },
            INTEGER_CONST => {
                let num = self.current_token.clone();
                self.eat(INTEGER_CONST)?;
                Ok(Node::NumOp(num))
            },
            REAL_CONST => {
                let num = self.current_token.clone();
                self.eat(REAL_CONST)?;
                Ok(Node::NumOp(num))
            },
            LPAREN => {
                self.eat(LPAREN)?;
                let node = self.expr()?;
                self.eat(RPAREN)?;
                Ok(node)
            },
            ID => {
                self.variable()
            },
            _ => {
                return Err(ErrorType::InvalidTokenType(self.current_token.token_type.clone()))
            }
        }
    }

    /// term: factor ((MUL | INTEGER_DIV | FLOAT_DIV) factor)*
    fn term(&mut self) -> Result<Node, ErrorType> {
        let mut node = self.factor()?;

        while vec![MUL, INTEGER_DIV, FLOAT_DIV].contains(&self.current_token.token_type) {
            let token = self.current_token.clone();
            match token.token_type {
                MUL => self.eat(MUL)?,
                INTEGER_DIV => self.eat(INTEGER_DIV)?,
                FLOAT_DIV => self.eat(FLOAT_DIV)?,
                _   => unreachable!(),
            }
            node = Node::BinaryOp(Box::new(node), token, Box::new(self.factor()?));
        }

        Ok(node)
    }

    /// expr: term ((PLUS | MINUS) term )*
    fn expr(&mut self) -> Result<Node, ErrorType> {
        let mut node = self.term()?;

        while vec![PLUS, MINUS].contains(&self.current_token.token_type) {
            let token = self.current_token.clone();
            match token.token_type {
                PLUS    => self.eat(PLUS)?,
                MINUS   => self.eat(MINUS)?,
                _       => unreachable!(),
            }
            node = Node::BinaryOp(Box::new(node), token, Box::new(self.term()?));
        }

        Ok(node)
    }

    /// type_spec : INTEGER | REAL
    fn type_spec(&mut self) -> Result<Node, ErrorType> {
        let token = self.current_token.clone();
        if token.token_type == INTEGER {
            self.eat(INTEGER)?;
        } else {
            self.eat(REAL)?;
        }
        Ok(Node::TypeSpec(token))
    }

    /// variable_declaration : ID (COMMA ID)* COLON type_spec
    fn variable_declaration(&mut self) -> Result<Box<Vec<Node>>, ErrorType> {
        let mut var_nodes = vec![Node::Var(self.current_token.clone())];
        self.eat(ID)?;

        while self.current_token.token_type == COMMA {
            self.eat(COMMA)?;
            var_nodes.push(Node::Var(self.current_token.clone()));
            self.eat(ID)?;
        }

        self.eat(COLON)?;
        let type_node = self.type_spec()?;

        let nodes = var_nodes.into_iter().map(|n| {
            Node::VarDecl(Box::new(n), Box::new(type_node.clone()))
        }).collect();
        Ok(Box::new(nodes))
    }

    /// formal_parameters : ID (COMMA ID)* COLON type_spec
    fn formal_parameters(&mut self) -> Result<Vec<Node>, ErrorType> {
        let mut var_nodes = vec![Node::Var(self.current_token.clone())];
        self.eat(ID)?;

        while self.current_token.token_type == COMMA {
            self.eat(COMMA)?;
            var_nodes.push(Node::Var(self.current_token.clone()));
            self.eat(ID)?;
        }

        self.eat(COLON)?;
        let type_node = self.type_spec()?;

        let nodes = var_nodes.into_iter().map(|n| {
            Node::Param(Box::new(n), Box::new(type_node.clone()))
        }).collect();
        Ok(nodes)
    }

    /// formal_parameter_list : formal_parameters | formal_parameters SEMI formal_parameter_list
    fn formal_parameter_list(&mut self) -> Result<Box<Vec<Node>>, ErrorType> {
        if self.current_token.token_type != ID {
            return Ok(Box::new(vec![]))
        }

        let mut param_nodes = self.formal_parameters()?;
        while self.current_token.token_type == SEMI {
            self.eat(SEMI)?;
            let params = self.formal_parameters()?;
            param_nodes.extend(params);
        }

        Ok(Box::new(param_nodes))
    }

    /// declarations : VAR (variable_declaration SEMI)+ | (PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI)* | empty
    fn declarations(&mut self) -> Result<Box<Vec<Node>>, ErrorType> {
        let mut decs = vec![];

        loop {
            if self.current_token.token_type == VAR {
                self.eat(VAR)?;
                while self.current_token.token_type == ID {
                    let mut ds = self.variable_declaration()?;
                    decs.append(&mut ds);
                    self.eat(SEMI)?;
                }
            } else if self.current_token.token_type == PROCEDURE {
                self.eat(PROCEDURE)?;
                let proc_name = self.current_token.clone();
                self.eat(ID)?;
                let proc_params = if self.current_token.token_type == LPAREN {
                    self.eat(LPAREN)?;
                    let list = self.formal_parameter_list()?;
                    self.eat(RPAREN)?;
                    list
                } else {
                    Box::new(vec![])
                };
                self.eat(SEMI)?;
                let proc_block = Box::new(self.block()?);
                self.eat(SEMI)?;
                decs.push(Node::ProcedureDecl(proc_name, proc_block, proc_params));
            } else {
                break;
            }
        }

        Ok(Box::new(decs))
    }

    /// block : declarations compound_statement
    fn block(&mut self) -> Result<Node, ErrorType> {
        let decs = self.declarations()?;
        let cmpd = Box::new(self.compound_statement()?);
        Ok(Node::Block(decs, cmpd))
    }

    /// program : PROGRAM variable SEMI block DOT
    fn program(&mut self) -> Result<Node, ErrorType> {
        self.eat(PROGRAM)?;
        let name = self.current_token.clone();
        self.eat(ID)?;
        self.eat(SEMI)?;
        let block = Box::new(self.block()?);
        self.eat(DOT)?;
        Ok(Node::Program(name, block))
    }

    /// compound_statement: BEGIN statement_list END
    fn compound_statement(&mut self) -> Result<Node, ErrorType> {
        self.eat(BEGIN)?;
        let nodes = self.statement_list()?;
        self.eat(END)?;

        Ok(Node::Compound(nodes))
    }

    /// statement_list: statement | statement SEMI statement_list
    fn statement_list(&mut self) -> Result<Box<Vec<Node>>, ErrorType> {
        let mut list = vec![self.statement()?];
        while self.current_token.token_type == SEMI {
            self.eat(SEMI)?;
            list.push(self.statement()?);
        }

        let list = Box::new(list);

        if self.current_token.token_type == ID {
            Err(ErrorType::SyntaxError)
        } else {
            Ok(list)
        }
    }

    /// statement: compound_statement | assignment_statement | empty
    fn statement(&mut self) -> Result<Node, ErrorType> {
        if self.current_token.token_type == BEGIN {
            self.compound_statement()
        } else if self.current_token.token_type == ID {
            self.assignment_statement()
        } else {
            self.empty()
        }
    }

    /// assignment_statement: variable ASSIGN expr
    fn assignment_statement(&mut self) -> Result<Node, ErrorType> {
        let left = self.variable()?;
        let op = self.current_token.clone();
        self.eat(ASSIGN)?;
        let right = self.expr()?;
        Ok(Node::Assign(Box::new(left), op, Box::new(right)))
    }

    /// variable: ID
    fn variable(&mut self) -> Result<Node, ErrorType> {
        let node = Node::Var(self.current_token.clone());
        self.eat(ID)?;
        Ok(node)
    }

    /// empty:
    fn empty(&mut self) -> Result<Node, ErrorType> {
        Ok(Node::NoOp())
    }

    /// Parse the input with grammer
    ///
    /// program : PROGRAM variable SEMI block DOT
    /// block : declarations compound_statement
    /// declarations : (VAR (variable_declaration SEMI)+)*
    ///    | (PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI)*
    ///    | empty
    /// variable_declaration : ID (COMMA ID)* COLON type_spec
    /// formal_params_list : formal_parameters
    ///                    | formal_parameters SEMI formal_parameter_list
    /// formal_parameters : ID (COMMA ID)* COLON type_spec
    /// type_spec : INTEGER | REAL
    /// compound_statement : BEGIN statement_list END
    /// statement_list : statement
    ///                | statement SEMI statement_list
    /// statement : compound_statement
    ///           | assignment_statement
    ///           | empty
    /// assignment_statement : variable ASSIGN expr
    /// empty :
    /// expr : term ((PLUS | MINUS) term)*
    /// term : factor ((MUL | INTEGER_DIV | FLOAT_DIV) factor)*
    /// factor : PLUS factor
    ///        | MINUS factor
    ///        | INTEGER_CONST
    ///        | REAL_CONST
    ///        | LPAREN expr RPAREN
    ///        | variable
    /// variable: ID
    pub fn parse(&mut self) -> Result<Node, ErrorType> {
        let node = self.program()?;
        if self.current_token.token_type != EOF {
            Err(ErrorType::SyntaxError)
        } else {
            Ok(node)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn assign() {
        let lexer = Lexer::new("a := 23");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.assignment_statement().expect("parse failed!");
    }

    #[test]
    fn compound() {
        let lexer = Lexer::new("BEGIN END");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.compound_statement().expect("parse failed!");
    }

    #[test]
    fn compound_with_assignments() {
        let lexer = Lexer::new("BEGIN a:= 3; b := 23; c := a + b; END");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.compound_statement().expect("parse failed!");
    }

    #[test]
    fn variable_declaration() {
        let lexer = Lexer::new("a,b,c:INTEGER");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.variable_declaration().expect("parse failed!");
    }

    #[test]
    fn invalid_syntax() {
        let lexer = Lexer::new("BEGIN a := 10 * ; END");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let stmt = parser.compound_statement();
        assert!(stmt.is_err());
        assert_eq!(Some(ErrorType::InvalidTokenType(TokenType::SEMI)), stmt.err());
    }

    #[test]
    fn declarations() {
        let lexer = Lexer::new("VAR a,b,c:INTEGER;e,f:REAL;");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.declarations().expect("parse failed!");
    }

    #[test]
    fn blocks() {
        let test_block = |input| {
            let lexer = Lexer::new(input);
            let mut parser = Parser::new(lexer).expect("invalid syntax");
            let _ = parser.block().expect("parse failed!");
        };
        test_block("BEGIN END");
        test_block("VAR a,b,c:INTEGER;e,f:REAL; BEGIN END");
        test_block("VAR a : INTEGER; BEGIN a := 23 END");
        test_block("VAR a, b : INTEGER; BEGIN a := 23; b := 2 + a; END");
        test_block("VAR a:INTEGER; VAR c:REAL; VAR d:INTEGER; BEGIN a := 23; b := 2 + a; END");
    }

    #[test]
    fn procedure() {
        let lexer = Lexer::new("PROGRAM progname; VAR a : INTEGER; PROCEDURE P1; BEGIN END; BEGIN a := 23 END.");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.program().expect("parse failed!");
    }

    #[test]
    fn procedure_with_params() {
        let lexer = Lexer::new("
            PROGRAM progname;
                PROCEDURE P1(); BEGIN END;
                PROCEDURE P2(a: INTEGER); BEGIN END;
                PROCEDURE P3(a,b,c: INTEGER); BEGIN END;
                PROCEDURE P4(a,b,c: INTEGER; d,e: REAL); BEGIN END;
            BEGIN END.");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.program().expect("parse failed!");
    }

    #[test]
    fn program() {
        let lexer = Lexer::new("PROGRAM progname; VAR a : INTEGER; BEGIN a := 23 END.");
        let mut parser = Parser::new(lexer).expect("invalid syntax");
        let _ = parser.program().expect("parse failed!");
    }
}
