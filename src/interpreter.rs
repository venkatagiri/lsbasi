use std::collections::BTreeMap;
use std::fmt;

use parser::Node;
use lexer::TokenType::*;
use error::ErrorType;

/// Symbols
#[derive(Clone)]
enum Symbol {
    BuiltinSymbol(String),
    VarSymbol(String, Box<Symbol>),
    ProcedureSymbol(String, Box<Vec<Symbol>>),
}

impl Symbol {
    fn name(&self) -> String {
        match *self {
            Symbol::BuiltinSymbol(ref name) => name.clone(),
            Symbol::VarSymbol(ref name, _) => name.clone(),
            Symbol::ProcedureSymbol(ref name, _) => name.clone(),
        }
    }
}

impl fmt::Debug for Symbol {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        match *self {
            Symbol::BuiltinSymbol(ref name) => write!(f, "<{}:BUILTIN>", name),
            Symbol::VarSymbol(ref name, ref symbol_type) => write!(f, "<{}:{}>", name, symbol_type.name()),
            Symbol::ProcedureSymbol(ref name, ref params) => {
                write!(f, "<{}:[", name)?;
                for param in (*params).iter() {
                    write!(f, "{:?},", param)?;
                }
                write!(f, "]>")
            }
        }
    }
}

/// Scoped Symbol Table
#[derive(Clone, Debug)]
pub struct ScopedSymbolTable {
    symbols: BTreeMap<String, Symbol>,
    scope_name: String,
    scope_level: u64,
    enclosing_scope: Option<String>,
}

impl ScopedSymbolTable {
    fn new(name: String, level: u64, enclosing: Option<String>) -> ScopedSymbolTable {
        ScopedSymbolTable {
            symbols: BTreeMap::new(),
            scope_name: name,
            scope_level: level,
            enclosing_scope: enclosing,
        }
    }

    fn builtin_symbols(&mut self) {
        self.define(Symbol::BuiltinSymbol("INTEGER".into())).expect("couldn't define symbol");
        self.define(Symbol::BuiltinSymbol("REAL".into())).expect("couldn't define symbol");
    }

    fn define(&mut self, s: Symbol) -> Result<(), ErrorType> {
        if self.symbols.contains_key(&s.name()) {
            return Err(ErrorType::DuplicateDecl(s.name()))
        }
        self.symbols.insert(s.name(), s);
        Ok(())
    }

    fn lookup(&self, name: &str) -> Option<Symbol> {
        self.symbols.get(name).cloned()
    }
}

/// Semantic Analyzer
#[derive(Debug)]
pub struct SemanticAnalyzer {
    current_scope: Option<String>,
    pub scopes: BTreeMap<String, ScopedSymbolTable>,
}

impl SemanticAnalyzer {
    pub fn new() -> SemanticAnalyzer {
        SemanticAnalyzer {
            current_scope: None,
            scopes: BTreeMap::new(),
        }
    }

    fn lookup_scope(&mut self, scope_name: &str, name: &str) -> Option<Symbol> {
        let mut enclosing = None;
        let res = self.scopes.get(scope_name).and_then(|ref s| {
            enclosing = s.enclosing_scope.clone();
            s.lookup(name)
        });
        res.or_else(||
            enclosing.and_then(|ref par_name|
                self.lookup_scope(par_name, name)
            )
        )
    }

    fn lookup(&mut self, name: &str) -> Option<Symbol> {
        let scope_name = self.current_scope.clone().expect("missing scope");
        self.lookup_scope(&scope_name, name)
    }

    fn define(&mut self, s: Symbol) -> Result<(), ErrorType> {
        let scope_name = self.current_scope.clone().expect("missing scope");
        if let Some(scope) = self.scopes.get_mut(&scope_name) {
            scope.define(s)?;
        }
        Ok(())
    }

    fn change_scope(&mut self, scope_name: &String) {
        // add new scope for procedure
        let (curr_scope_name, curr_scope_level) = {
            let scope_name = self.current_scope.clone().expect("missing scope");
            let ref cur = self.scopes[&scope_name];
            (cur.scope_name.clone(), cur.scope_level)
        };
        let new_scope = ScopedSymbolTable::new(
            scope_name.clone(),
            curr_scope_level + 1,
            Some(curr_scope_name),
        );
        self.scopes.insert(scope_name.clone(), new_scope);
        self.current_scope = Some(scope_name.clone());
    }

    fn change_to_enclosing_scope(&mut self) {
        self.current_scope = {
            let scope_name = self.current_scope.clone().expect("missing scope");
            let ref cur = self.scopes[&scope_name];
            cur.enclosing_scope.clone()
        };
    }

    pub fn visit(&mut self, node: &Node) -> Result<(), ErrorType> {
        Ok(match *node {
            Node::Program(ref _token, ref node) => {
                self.current_scope = Some("global".into());
                let mut sst = ScopedSymbolTable::new("global".into(), 1, None);
                sst.builtin_symbols();
                self.scopes.insert("global".into(), sst);
                self.visit(node)?;
                self.current_scope = None;
            },
            Node::ProcedureDecl(ref name, ref node, ref params) => {
                let proc_name = name.value();
                self.change_scope(&proc_name);

                let mut proc_params = vec![];
                for param in (*params).iter() {
                    match *param {
                        Node::Param(ref var_node, ref type_node) => {
                            let type_name = type_node.unwrap().value();
                            let type_symbol = self.lookup(&type_name).ok_or(ErrorType::NameError(type_name))?;
                            let var_name = var_node.unwrap().value();
                            let var_symbol = Symbol::VarSymbol(var_name, Box::new(type_symbol));
                            self.define(var_symbol.clone())?;
                            proc_params.push(var_symbol);
                        },
                        _ => unreachable!(),
                    }
                }
                let proc_symbol = Symbol::ProcedureSymbol(proc_name.clone(), Box::new(proc_params));

                // visit block in procedure with new scope
                self.visit(&*node)?;

                self.change_to_enclosing_scope();
                self.define(proc_symbol)?;
            },
            Node::Param(ref _var_node, ref _type_node) => {},
            Node::Block(ref decs, ref cmpd) => {
                for node in (*decs).iter() {
                    self.visit(&*node)?;
                }
                self.visit(&*cmpd)?;
            },
            Node::VarDecl(ref var_node, ref type_node) => {
                let type_name = type_node.unwrap().value();
                let type_symbol = self.lookup(&type_name).ok_or(ErrorType::NameError(type_name))?;
                let var_name = var_node.unwrap().value();
                let var_symbol = Symbol::VarSymbol(var_name, Box::new(type_symbol));
                self.define(var_symbol)?;
            },
            Node::TypeSpec(ref _token) => {},
            Node::Compound(ref nodes) => {
                for node in (*nodes).iter() {
                    self.visit(&*node)?;
                }
            },
            Node::Assign(ref left, ref _op, ref right) => {
                let var_name = left.unwrap().value();
                let _ = self.lookup(&var_name).ok_or(ErrorType::NameError(var_name))?;
                self.visit(&*right)?;
            },
            Node::NoOp() => {},
            Node::BinaryOp(ref left, ref _op, ref right) => {
                self.visit(&*left)?;
                self.visit(&*right)?;
            },
            Node::UnaryOp(ref _op, ref node) => {
                self.visit(&*node)?;
            },
            Node::NumOp(ref _node) => {},
            Node::Var(ref node) => {
                let var_name = node.value();
                let _ = self.lookup(&var_name).ok_or(ErrorType::NameError(var_name))?;
            },
        })
    }
}

/// Interpreter
pub struct Interpreter {
    pub global_memory: BTreeMap<String, f64>,
}

impl Interpreter {
    pub fn new() -> Interpreter {
        Interpreter {
            global_memory: BTreeMap::new(),
        }
    }

    fn value(&mut self, node: &Node) -> f64 {
        match *node {
            Node::BinaryOp(ref left, ref op, ref right) => {
                match op.token_type {
                    PLUS        => self.value(&*left) + self.value(&*right),
                    MINUS       => self.value(&*left) - self.value(&*right),
                    MUL         => self.value(&*left) * self.value(&*right),
                    INTEGER_DIV => (self.value(&*left) as i64 / self.value(&*right) as i64) as f64,
                    FLOAT_DIV   => self.value(&*left) / self.value(&*right),
                    _           => unreachable!(),
                }
            },
            Node::UnaryOp(ref op, ref node) => {
                match op.token_type {
                    PLUS    => self.value(&*node),
                    MINUS   => -self.value(&*node),
                    _       => unreachable!(),
                }
            },
            Node::NumOp(ref node) => {
                node.to_number()
            },
            Node::Var(ref node) => {
                let key = node.value();
                self.global_memory.get(&key).cloned().unwrap_or_default()
            },
            _ => unreachable!(),
        }
    }

    fn visit(&mut self, node: &Node) {
        match *node {
            Node::Program(ref _token, ref node) => {
                self.visit(node);
            },
            Node::ProcedureDecl(ref _token, ref _node, ref _params) => {},
            Node::Block(ref decs, ref cmpd) => {
                for node in (*decs).iter() {
                    self.visit(&*node);
                }
                self.visit(&*cmpd);
            },
            Node::VarDecl(ref _var_node, ref _type_node) => {},
            Node::TypeSpec(ref _token) => {},
            Node::Compound(ref nodes) => {
                for node in (*nodes).iter() {
                    self.visit(&*node);
                }
            },
            Node::Assign(ref left, ref _op, ref right) => {
                match **left {
                    Node::Var(ref node) => {
                        let key = node.value();
                        let value = self.value(right);
                        let content = self.global_memory.entry(key).or_insert(value);
                        *content = value;
                    },
                    _ => unreachable!(),
                }
            },
            Node::NoOp() => {},
            _ => unreachable!(),
        }
    }

    pub fn interpret(&mut self, tree: &Node) {
        self.visit(&tree)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use ::lexer::Lexer;
    use ::parser::Parser;

    fn interpret(input: &str) -> Result<Interpreter, ErrorType> {
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer)?;
        let tree = parser.parse()?;
        let mut stb = SemanticAnalyzer::new();
        stb.visit(&tree)?;
        let mut int = Interpreter::new();
        int.interpret(&tree);
        Ok(int)
    }

    #[test]
    fn basic() {
        let interpreter = interpret("PROGRAM progname; VAR a : REAL; BEGIN a := 23 END.").expect("interpret failed!");
        assert_eq!(interpreter.global_memory.get("a"), Some(&23f64));
    }

    #[test]
    fn arithmetic() {
        for (expr, result) in vec![
            ("3", 3),
            ("2 + 7 * 4", 30),
            ("7 - 8 DIV 4", 5),
            ("14 + 2 * 3 - 6 DIV 2", 17),
            ("7 + 3 * (10 DIV (12 DIV (3 + 1) - 1))", 22),
            ("7 + 3 * (10 DIV (12 DIV (3 + 1) - 1)) DIV (2 + 3) - 5 - 3 + (8)", 10),
            ("7 + (((3 + 2)))", 12),
            ("- 3", -3),
            ("+ 3", 3),
            ("5 - - - + - 3", 8),
            ("5 - - - + - (3 + 4) - +2", 10),
        ] {
            let prog = format!("PROGRAM progname; VAR a : REAL; BEGIN a := {} END.", expr);
            let interpreter = interpret(&prog).expect("interpret failed!");
            assert_eq!(interpreter.global_memory.get("a"), Some(&(result as f64)));
        }
    }

    #[test]
    fn name_error() {
        let int = interpret("PROGRAM progname; BEGIN a := 10;  END.");
        assert!(int.is_err());
        assert_eq!(Some(ErrorType::NameError("a".into())), int.err());
    }

    #[test]
    fn duplicate_decl() {
        let int = interpret("PROGRAM progname; VAR a, a : INTEGER; BEGIN  END.");
        assert!(int.is_err());
        assert_eq!(Some(ErrorType::DuplicateDecl("a".into())), int.err());
    }

    #[test]
    fn procedure_scopes() {
        let int = interpret("
            PROGRAM Main;
                VAR x, y: REAL;

                PROCEDURE Alpha(a : INTEGER);
                    VAR y : INTEGER;
                BEGIN
                    x := x + y + a;
                END;

            BEGIN { Main }
            END.  { Main }
        ");
        assert!(int.is_ok());
    }

    #[test]
    fn multi_procs() {
        let int = interpret("
            PROGRAM Main;
                VAR x, y: REAL;

                PROCEDURE Alpha(a : INTEGER);
                    VAR y : INTEGER;
                BEGIN
                    x := x + y + a;
                END;

                PROCEDURE Beta(c : REAL);
                    VAR y : INTEGER;
                BEGIN
                    x := x + y + c;
                END;

            BEGIN { Main }
            END.  { Main }
        ");
        assert!(int.is_ok());
    }
}
