use lexer::{Token, TokenType};

/// ErrorType
#[derive(Debug, PartialEq)]
pub enum ErrorType {
    InvalidChar,
    SyntaxError,
    InvalidToken(TokenType, Token),
    InvalidTokenType(TokenType),
    NameError(String),
    DuplicateDecl(String),
}
