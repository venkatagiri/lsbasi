use parser::Node;
use lexer::TokenType;

/// Source 2 Source Compiler
#[allow(dead_code)]
fn compile(node: &Node) -> String {
    match *node {
        Node::Program(ref name, ref block) => {
            format!("PROGRAM {};\n\
                {}. {{End of {}}}", name.value(), compile(block), name.value())
        },
        Node::ProcedureDecl(ref name, ref block, ref params) => {
            let mut out = format!("PROCEDURE {}(", name.value());
            let mut param_str = (*params).iter().map(|param|
                compile(&param)
            ).collect::<String>();
            param_str.pop(); // remove semi-colon
            param_str.pop(); // remove trailing space
            out.push_str(&param_str);
            out.push_str(&format!(");\n{}; {{End of {}}}", compile(block), name.value()));
            out.lines().map(|l| format!("    {}\n", l) ).collect()
        },
        Node::Block(ref decs, ref cmpd) => {
            let mut out = String::new();
            for node in (*decs).iter() {
                out.push_str(&compile(node));
            }
            out.push_str(&compile(cmpd));
            out
        },
        Node::Param(ref var_node, ref type_node) => {
            format!("{} : {}; ", var_node.unwrap().value(), compile(&type_node))
        },
        Node::VarDecl(ref var_node, ref type_node) => {
            format!("    VAR {} : {};\n", var_node.unwrap().value(), compile(&type_node))
        },
        Node::TypeSpec(ref token) => {
            token.value()
        },
        Node::Compound(ref nodes) => {
            let mut out = String::new();
            out.push_str("BEGIN\n");
            for node in (*nodes).iter() {
                let res = compile(node);
                if !res.is_empty() {
                    out.push_str(&format!("    {}", res));
                }
            }
            out.push_str("END");
            out
        },
        Node::Assign(ref left, ref _op, ref right) => {
            match **left {
                Node::Var(ref node) => {
                    format!("{} := {};\n", node.value(), compile(right))
                },
                _ => unreachable!(),
            }
        },
        Node::BinaryOp(ref left, ref op, ref right) => {
            let left = compile(left);
            let right = compile(right);
            let op = match op.token_type{
                TokenType::PLUS        => "+",
                TokenType::MINUS       => "-",
                TokenType::MUL         => "*",
                TokenType::INTEGER_DIV => "/",
                TokenType::FLOAT_DIV   => "DIV",
                _           => unreachable!(),
            };
            format!("{} {} {}", left, op, right)
        },
        Node::UnaryOp(ref op, ref node) => {
            let op = match op.token_type{
                TokenType::PLUS    => "",
                TokenType::MINUS   => " - ",
                _       => unreachable!(),
            };
            format!("{}{}", op, compile(&*node))
        },
        Node::NumOp(ref node) |
        Node::Var(ref node) => {
            node.value()
        },
        Node::NoOp() => {
            "".into()
        },
    }
}

#[cfg(test)]
mod tests {
    use super::compile;
    use ::lexer::Lexer;
    use ::parser::Parser;

    #[test]
    fn check() {
        let prog = "\
PROGRAM Main;
    VAR x: INTEGER;

    PROCEDURE Alpha(a: INTEGER; b: REAL);
    BEGIN
        x := a + b;
    END;
BEGIN
END.
        ";
        let expected_out = "\
PROGRAM Main;
    VAR x : INTEGER;
    PROCEDURE Alpha(a : INTEGER; b : REAL);
    BEGIN
        x := a + b;
    END; {End of Alpha}
BEGIN
END. {End of Main}\
        ";

        let lexer = Lexer::new(prog);
        let mut parser = Parser::new(lexer).expect("parse failed!");
        let tree = parser.parse().expect("parse failed!");
        let out = compile(&tree);
        assert_eq!(expected_out, out);
    }
}