use std::fmt;
use error::ErrorType;

/// Tokens
#[derive(Debug, PartialEq, Clone)]
#[allow(non_camel_case_types)]
pub enum TokenType {
    PROGRAM,
    PROCEDURE,
    VAR,
    COMMA,
    COLON,
    INTEGER,
    REAL,
    INTEGER_CONST,
    REAL_CONST,
    INTEGER_DIV,
    FLOAT_DIV,
    BEGIN,
    END,
    DOT,
    ASSIGN,
    SEMI,
    ID,
    PLUS,
    MINUS,
    MUL,
    LPAREN,
    RPAREN,
    EOF,
}
use self::TokenType::*;

#[derive(Clone, PartialEq)]
pub struct Token {
    pub token_type: TokenType,
    value: Option<String>,
}

impl Token {
    fn new(token_type: TokenType, value: Option<String>) -> Token {
        Token {
            token_type: token_type,
            value: value,
        }
    }

    pub fn to_number(&self) -> f64 {
        if self.token_type == INTEGER_CONST {
            self.value.clone().unwrap().parse::<i64>().unwrap() as f64
        } else {
            self.value.clone().unwrap().parse().unwrap()
        }
    }

    pub fn value(&self) -> String {
        self.value.clone().unwrap_or("".to_string())
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "Token({:?}, {:?})", self.token_type, self.value)
    }
}

impl fmt::Debug for Token {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "Token({:?}, {:?})", self.token_type, self.value)
    }
}

/// Lexer
#[derive(Clone, Debug)]
pub struct Lexer {
    text: Vec<u8>,
    pos: usize,
    current_char: Option<char>,
    reserved_keywords: Vec<(TokenType, &'static str)>,
}

impl Lexer {
    pub fn new(text: &str) -> Lexer {
        let keywords = vec![
            (PROGRAM, "PROGRAM"),
            (PROCEDURE, "PROCEDURE"),
            (VAR, "VAR"),
            (INTEGER_DIV, "DIV"),
            (INTEGER, "INTEGER"),
            (REAL, "REAL"),
            (BEGIN, "BEGIN"),
            (END, "END"),
        ];
        Lexer {
            text: text.bytes().collect(),
            pos: 0,
            current_char: Some(text.bytes().collect::<Vec<_>>().first().unwrap().clone() as char),
            reserved_keywords: keywords,
        }
    }

    /// Peek at the next char
    fn peek(&self) -> Option<char> {
        let peek_pos = self.pos + 1;
        if peek_pos > self.text.len() - 1 {
            None
        } else {
            Some(*self.text.get(peek_pos).expect("invalid pos") as char)
        }
    }

    /// Handles identifiers and keywords
    fn _id(&mut self) -> Token {
        let mut res = String::new();
        while let Some(ch) = self.current_char {
            if !ch.is_alphanumeric() {
                break;
            }
            res.push(ch);
            self.advance();
        }

        // reserved keywords
        if let Some(pos) = self.reserved_keywords.iter().position(|&(_, ref v)| *v == res.to_uppercase() ) {
            let (k, v) = self.reserved_keywords[pos].clone();
            Token::new(k, Some(v.to_string()))
        } else {
            Token::new(ID, Some(res))
        }
    }

    /// Advance the `pos` pointer and set the `current_char` variable
    fn advance(&mut self) {
        self.pos += 1;
        if self.pos > self.text.len() - 1 {
            self.current_char = None;
        } else {
            self.current_char = Some(*self.text.get(self.pos).expect("invalid pos") as char);
        }
    }

    /// Skip the whitespace
    fn skip_whitespace(&mut self) {
        while let Some(ch) = self.current_char {
            if !ch.is_whitespace() {
                break;
            }
            self.advance();
        }
    }

    /// Skip the comment section
    fn skip_comment(&mut self) {
        while let Some(ch) = self.current_char {
            if ch == '}' {
                break;
            }
            self.advance();
        }
        self.advance(); // skip }
    }

    /// Return a (multidigit) integer or float consumed from the input
    fn number(&mut self) -> Token {
        let mut res = String::new();
        while let Some(ch) = self.current_char {
            if !ch.is_digit(10) {
                break;
            }
            res.push(ch);
            self.advance();
        }
        if self.current_char == Some('.') {
            res.push('.');
            self.advance();
            while let Some(ch) = self.current_char {
                if !ch.is_digit(10) {
                    break;
                }
                res.push(ch);
                self.advance();
            }
            Token::new(REAL_CONST, Some(res))
        } else {
            Token::new(INTEGER_CONST, Some(res))
        }
    }

    /// Lexical analyzer (also known as scanner or tokenizer)
    /// This method is responsible for breaking a sentence
    /// apart into tokens. One token at a time.
    pub fn get_next_token(&mut self) -> Result<Token, ErrorType> {
        while let Some(ch) = self.current_char {
            if ch.is_whitespace() {
                self.skip_whitespace();
                continue;
            }
            if ch.is_digit(10) {
                return Ok(self.number());
            }
            if ch == '{' {
                self.advance();
                self.skip_comment();
                continue;
            }
            if ch.is_alphabetic() {
                return Ok(self._id());
            }
            if ch == ':' && self.peek() == Some('=') {
                self.advance();
                self.advance();
                return Ok(Token::new(ASSIGN, Some(":=".to_string())));
            }
            if ch == ':'{
                self.advance();
                return Ok(Token::new(COLON, Some(":".to_string())));
            }
            if ch == ','{
                self.advance();
                return Ok(Token::new(COMMA, Some(",".to_string())));
            }
            if ch == ';'{
                self.advance();
                return Ok(Token::new(SEMI, Some(";".to_string())));
            }
            if ch == '.'{
                self.advance();
                return Ok(Token::new(DOT, Some(".".to_string())));
            }
            if ch == '+' {
                self.advance();
                return Ok(Token::new(PLUS, Some("+".to_string())));
            }
            if ch == '-' {
                self.advance();
                return Ok(Token::new(MINUS, Some("-".to_string())));
            }
            if ch == '*' {
                self.advance();
                return Ok(Token::new(MUL, Some("*".to_string())));
            }
            if ch == '/' {
                self.advance();
                return Ok(Token::new(FLOAT_DIV, Some("/".to_string())));
            }
            if ch == '(' {
                self.advance();
                return Ok(Token::new(LPAREN, Some("(".to_string())));
            }
            if ch == ')' {
                self.advance();
                return Ok(Token::new(RPAREN, Some(")".to_string())));
            }
            return Err(ErrorType::InvalidChar);
        }
        Ok(Token::new(EOF, None))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn _token(tt: TokenType, val: &str) -> Token {
        Token {
            token_type: tt,
            value: Some(val.to_string()),
        }
    }

    #[test]
    fn advance() {
        let mut lexer = Lexer::new("3+4");
        assert_eq!(lexer.pos, 0);
        lexer.advance();
        lexer.advance();
        assert_eq!(lexer.current_char, Some('4'));
        lexer.advance();
        assert_eq!(lexer.current_char, None);
    }

    #[test]
    fn skip_ws() {
        let mut lexer = Lexer::new("  3    +4");
        assert_eq!(lexer.current_char, Some(' '));
        lexer.skip_whitespace();
        assert_eq!(lexer.current_char, Some('3'));
        lexer.advance();
        lexer.skip_whitespace();
        assert_eq!(lexer.current_char, Some('+'));
    }

    #[test]
    fn skip_comments() {
        let mut lexer = Lexer::new("  3{comment}+4");
        assert_eq!(lexer.current_char, Some(' '));
        lexer.skip_whitespace();
        assert_eq!(lexer.current_char, Some('3'));
        lexer.advance();
        lexer.skip_comment();
        assert_eq!(lexer.current_char, Some('+'));
    }

    #[test]
    fn get_next_token() {
        let mut lexer = Lexer::new("3+4");
        assert_eq!(lexer.pos, 0);
        let token = lexer.get_next_token().expect("failed to get token");
        assert_eq!(token, _token(INTEGER_CONST, "3"));
        let token = lexer.get_next_token().expect("failed to get token");
        assert_eq!(token, _token(PLUS, "+"));
        let token = lexer.get_next_token().expect("failed to get token");
        assert_eq!(token, _token(INTEGER_CONST, "4"));
    }

    #[test]
    fn identifiers() {
        fn next_token(l: &mut Lexer) -> Token {
            l.get_next_token().expect("failed to get token")
        }
        let mut lexer = Lexer::new("BEGIN a := 2; END.");
        assert_eq!(next_token(&mut lexer), _token(BEGIN, "BEGIN"));
        assert_eq!(next_token(&mut lexer), _token(ID, "a"));
        assert_eq!(next_token(&mut lexer), _token(ASSIGN, ":="));
        assert_eq!(next_token(&mut lexer), _token(INTEGER_CONST, "2"));
        assert_eq!(next_token(&mut lexer), _token(SEMI, ";"));
        assert_eq!(next_token(&mut lexer), _token(END, "END"));
        assert_eq!(next_token(&mut lexer), _token(DOT, "."));
        assert_eq!(next_token(&mut lexer), Token::new(EOF, None));
    }

    #[test]
    fn just_id() {
        fn next_token(l: &mut Lexer) -> Token {
            l.get_next_token().expect("failed to get token")
        }
        let mut lexer = Lexer::new("index");
        assert_eq!(next_token(&mut lexer), _token(ID, "index"));
        assert_eq!(next_token(&mut lexer), Token::new(EOF, None));
    }
}